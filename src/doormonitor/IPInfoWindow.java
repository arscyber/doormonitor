/**
 * � Copyright ArsCyber Trademark
 *	 All rights reserved.
 */


package doormonitor;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import java.io.IOException;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import javafx.scene.image.Image;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class IPInfoWindow {
    
    private static void playSound(String soundName)
    {
        try
        {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(new File(soundName)));
            clip.start();
        }
        catch (Exception exc)
        {
            exc.printStackTrace(System.out);

        }
    }
    
    public static void show(String ipAddress) throws IOException{

        LookupService cl = new LookupService("GeoLiteCity.dat", LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);
        Location location = cl.getLocation(ipAddress);
        
        if (location != null)
        {
            
            playSound("confirmation.wav");
            
            String city = "Unidentified";
            String country = "Unidentified";
            String latitude = "Unidentified";
            String longitude = "Unidentified";
            
            if (location.countryName != null)
            {
                country = location.countryName;
            }
            else
            {
            	System.out.println("Country name is null.");
            }
            
            if (location.city != null)
            {
                city = location.city;
            }
            else
            {
            	System.out.println("City is null.");
            }
            
            String strIPGeoLocInfo = "IP: " + ipAddress + "\nCountry: " + country + "\nCity: " + city + "\nGeolocation: " + Float.toString(location.latitude) + " " + Float.toString(location.longitude);

            JOptionPane optionPane = new JOptionPane(strIPGeoLocInfo ,JOptionPane.INFORMATION_MESSAGE);
            JDialog dialog = optionPane.createDialog("IP information: copy to clipboard");
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Clipboard clipboard = toolkit.getSystemClipboard();
            StringSelection strSel = new StringSelection(strIPGeoLocInfo);
            //check if clipboard is null
            clipboard.setContents(strSel, null);   
        }
        else
        {
            playSound("error.wav");
            System.out.println("Location is null.");
        }
    }
}
